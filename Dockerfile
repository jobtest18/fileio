FROM openjdk:8-jre
ARG JAR_FILE
ADD target/${JAR_FILE} /usr/share/fileioapp/fileioapp.jar
CMD ["/usr/bin/java", "-jar", "/usr/share/fileioapp/fileioapp.jar"]

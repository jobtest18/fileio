# File IO Application

In order to run application, please, choose one of available options:

- `mvn spring-boot:run` to run locally as Spring Boot application
- `mvn clean package` to build applications' Docker image and to run it `docker run -p 8080:8080 bestcompany/fileio:0.0.1-SNAPSHOT`


Application was deployed as Docker image to Heroku.
Please, use [this link](https://fileioapp.herokuapp.com/) to view application in action

Expected archive could be downloaded using `wget`: `wget https://fileioapp.herokuapp.com/download --content-disposition`


package com.bestcompany.fileio.util;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import static org.junit.Assert.*;

public class ArchiveUtilsTest {
    @Rule
    public TemporaryFolder tmpFolder = new TemporaryFolder();

    @Test
    public void testZipArchiveDetection() {
        assertTrue(ArchiveUtils.isZipArchive("archive.zip"));
        assertTrue(ArchiveUtils.isZipArchive("archive.tar.gz.zip"));

        assertFalse(ArchiveUtils.isZipArchive(null));
        assertFalse(ArchiveUtils.isZipArchive("somefile.txt"));
        assertFalse(ArchiveUtils.isZipArchive("archive.tar.gz"));
    }

    @Test
    public void testTarGZArchiveDetection() {
        assertTrue(ArchiveUtils.isTarGzArchive("archive.tar.gz"));
        assertTrue(ArchiveUtils.isTarGzArchive("archive.zip.tar.gz"));

        assertFalse(ArchiveUtils.isTarGzArchive(null));
        assertFalse(ArchiveUtils.isTarGzArchive("somefile.txt"));
        assertFalse(ArchiveUtils.isTarGzArchive("archive.zip"));
    }

    @Test
    public void testZipToTar() throws IOException {
        File zipArchive = TestUtils.getTestFile("test.zip");
        File outputTar = tmpFolder.newFile("result.tar");
        String outputTarPath = outputTar.getAbsolutePath();

        try (FileInputStream inputStream = new FileInputStream(zipArchive);
             TarArchiveOutputStream tarStream = new TarArchiveOutputStream(new FileOutputStream(outputTar))) {

            ArchiveUtils.zipToTar(inputStream, tarStream);
            tarStream.flush();
        }

        try (TarArchiveInputStream inputStream = new TarArchiveInputStream(new FileInputStream(new File(outputTarPath)))) {
            TarArchiveEntry entry;
            int count = 0;

            while ((entry = inputStream.getNextTarEntry()) != null) {
                count++;
            }

            assertEquals(count, 3);
        }

    }

    @Test
    public void testGzipTartoTar() throws IOException{
        File tarGzArchive = TestUtils.getTestFile("test.tar.gz");
        File outputTar = tmpFolder.newFile("result.tar");
        String outputTarPath = outputTar.getAbsolutePath();

        try (FileInputStream inputStream = new FileInputStream(tarGzArchive);
             TarArchiveOutputStream tarStream = new TarArchiveOutputStream(new FileOutputStream(outputTar))) {

            ArchiveUtils.gzipTarToTar(inputStream, tarStream);
            tarStream.flush();
        }

        try (TarArchiveInputStream inputStream = new TarArchiveInputStream(new FileInputStream(new File(outputTarPath)))) {
            TarArchiveEntry entry;
            int count = 0;

            while ((entry = inputStream.getNextTarEntry()) != null) {
                count++;
            }

            assertEquals(count, 3);
        }
    }
}

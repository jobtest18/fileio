package com.bestcompany.fileio.util;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.assertEquals;


@Category(IntegrationTest.class)
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class DownloadControllerTest {

    @Autowired
    public TestRestTemplate restTemplate;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(8089);

    public void initMockServer() {

        stubFor(get(urlEqualTo("/files/test.zip"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/zip")
                        .withBodyFile("test.zip")));

        stubFor(get(urlEqualTo("/files/test.tar.gz"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/gzip")
                        .withBodyFile("test.tar.gz")));
    }

    @Test
    public void testMainPage() {
        assertEquals(HttpStatus.OK, restTemplate.getForEntity("/", String.class).getStatusCode());
    }

    @Test
    public void testArchiveDownload() throws IOException {
        initMockServer();

        HttpHeaders headers = new HttpHeaders();
        UriComponentsBuilder builder = UriComponentsBuilder.fromPath("/download")
                .queryParam("urls", "http://localhost:8089/files/test.zip", "http://localhost:8089/files/test.tar.gz");

        HttpEntity<?> entity = new HttpEntity<>(headers);

        ResponseEntity<Resource> response = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                entity,
                Resource.class);

        try (GzipCompressorInputStream gzipStream = new GzipCompressorInputStream(response.getBody().getInputStream());
             TarArchiveInputStream tarStream  = new TarArchiveInputStream(gzipStream)) {

            int count = 0;
            TarArchiveEntry entry;
            while ((entry = tarStream.getNextTarEntry()) != null) {
                count++;
            }

            assertEquals(count, 6);
        }
    }
}
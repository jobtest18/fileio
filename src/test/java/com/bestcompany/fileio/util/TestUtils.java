package com.bestcompany.fileio.util;

import java.io.File;

public class TestUtils {
    public static File getTestFile(String filename) {
        ClassLoader classLoader = TestUtils.class.getClassLoader();
        return new File(classLoader.getResource(filename).getFile());
    }
}

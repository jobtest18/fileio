package com.bestcompany.fileio.service;

import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import static com.bestcompany.fileio.util.ArchiveUtils.*;

@Component
public class DownloadServiceImpl implements DownloadService {

    public void downloadAsTarGzArchive(List<String> urls, OutputStream outputStream) throws IOException {
        try (GzipCompressorOutputStream gzipCompressorOutputStream = new GzipCompressorOutputStream(outputStream)) {
            downloadAsTarArchive(urls, gzipCompressorOutputStream);
        }
    }

    public void downloadAsTarArchive(List<String> urls, OutputStream outputStream) throws IOException {
        try (TarArchiveOutputStream tarArchiveOutputStream = new TarArchiveOutputStream(outputStream)) {
            tarArchiveOutputStream.setLongFileMode(TarArchiveOutputStream.LONGFILE_POSIX);

            for (String url : urls) {
                UrlResource urlResource = new UrlResource(url);
                String filename = urlResource.getFilename();

                if (isZipArchive(filename)) {
                    zipToTar(urlResource.getInputStream(), tarArchiveOutputStream);
                } else if (isTarGzArchive(filename)) {
                    gzipTarToTar(urlResource.getInputStream(), tarArchiveOutputStream);
                }

                tarArchiveOutputStream.flush();
            }
        }
    }

}

package com.bestcompany.fileio.service;

import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

@Service
public interface DownloadService {

    /**
     * Download specified urls and return their content as combined tar.gz archive
     *
     * @param urls list of urls for download
     * @param outputStream output stream which be used for writing resulting archive
     * @throws IOException
     */
    void downloadAsTarGzArchive(List<String> urls, OutputStream outputStream) throws IOException;

    /**
     * Download specified urls and return their content as combined tar archive
     *
     * @param urls list of urls for download
     * @param outputStream output stream which be used for writing resulting archive
     * @throws IOException
     */
    void downloadAsTarArchive(List<String> urls, OutputStream outputStream) throws IOException;
}

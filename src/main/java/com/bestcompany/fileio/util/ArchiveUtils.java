package com.bestcompany.fileio.util;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;

import java.io.IOException;
import java.io.InputStream;


/**
 * Class with utility functions for dealing with archives stream transformation
 */
public class ArchiveUtils {
    final private static int BUFFER_SIZE = 64000;

    private static void writeEntry(ArchiveOutputStream outputStream, ArchiveInputStream inputStream, ArchiveEntry sourceEntry, ArchiveEntry destEntry) throws IOException {
        outputStream.putArchiveEntry(destEntry);
        long entrySize = sourceEntry.getSize();

        byte[] buffer = new byte[BUFFER_SIZE];
        int offset = 0;
        while (offset < entrySize) {
            int nread = inputStream.read(buffer, 0, buffer.length);

            if (nread > 0) {
                outputStream.write(buffer, 0, nread);
            }

            if (nread == -1) {
                break;
            }

            offset += nread;
        }

        outputStream.closeArchiveEntry();
    }


    /**
     * Transform zip input stream into tar output stream
     *
     * @param inputStream  zip input stream
     * @param outputStream output tar stream
     * @throws IOException
     */
    public static void zipToTar(InputStream inputStream, TarArchiveOutputStream outputStream) throws IOException {
        try (ZipArchiveInputStream zipInputStream = new ZipArchiveInputStream(inputStream)) {

            ZipArchiveEntry inputEntry;
            while ((inputEntry = zipInputStream.getNextZipEntry()) != null) {
                TarArchiveEntry outputEntry = new TarArchiveEntry(inputEntry.getName());
                outputEntry.setSize(inputEntry.getSize());
                outputEntry.setModTime(inputEntry.getLastModifiedDate());
                writeEntry(outputStream, zipInputStream, inputEntry, outputEntry);
            }
        }
    }

    /**
     * Transform tar.gz input stream into tar output stream
     *
     * @param inputStream  tar.gz input stream
     * @param outputStream output tar stream
     * @throws IOException
     */
    public static void gzipTarToTar(InputStream inputStream, TarArchiveOutputStream outputStream) throws IOException {
        try (GzipCompressorInputStream gzipCompressorInputStream = new GzipCompressorInputStream(inputStream);
             TarArchiveInputStream tarArchiveInputStream = new TarArchiveInputStream(gzipCompressorInputStream)) {

            TarArchiveEntry tarArchiveEntry;
            while ((tarArchiveEntry = tarArchiveInputStream.getNextTarEntry()) != null) {
                writeEntry(outputStream, tarArchiveInputStream, tarArchiveEntry, tarArchiveEntry);
            }
        }
    }

    /**
     * Check is file a zip archive?
     *
     * @param filename
     * @return
     */
    public static boolean isZipArchive(String filename) {
        return filename != null && filename.endsWith(".zip");
    }


    /**
     * Check is file a tar.gz archive?
     *
     * @param filename
     * @return
     */
    public static boolean isTarGzArchive(String filename) {
        return filename != null && filename.endsWith(".tar.gz");
    }
}

package com.bestcompany.fileio.controller;


import com.bestcompany.fileio.service.DownloadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@RestController
public class DownloadController {
    @Value("#{'${fileio.default.urls}'.split(';')}")
    private List<String> defaultUrls;

    @Value("${fileio.default.outputFilename:result.tar.gz}")
    private String outputFilename;

    @Autowired
    private DownloadService downloadService;

    @GetMapping("/download")
    public void download(@RequestParam(required = false) List<String> urls, HttpServletResponse response) throws IOException {
        response.setContentType("application/gzip");
        response.setHeader("Content-disposition", "attachment;filename=" + outputFilename);

        List<String> urlsToDownload = CollectionUtils.isEmpty(urls) ? defaultUrls : urls;
        downloadService.downloadAsTarGzArchive(urlsToDownload, response.getOutputStream());

        response.flushBuffer();
    }

}
